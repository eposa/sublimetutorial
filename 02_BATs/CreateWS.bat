@echo off
setlocal enableextensions enabledelayedexpansion

:: CD to correct dir
cd /D "%~dp0"

:: IDEAS project root
echo Select IDEAS project folder (contains src folder)...

set projFolder=
set "psCommand="(new-object -COM 'Shell.Application').BrowseForFolder(0,'Please choose the IDEAS project root',0,0).self.path""
for /f "delims=" %%I in ('powershell %psCommand%') do (
	set tmpPath=%%I
	set projFolder=!projFolder!!tmpPath!
)

for /f %%i in ("%projFolder%") do (
	set projName=%%~ni
)

set escProjFolder=%projFolder:\=\\%

:: IDEAS project file
echo Select IDEAS .project file...
set xmlFile=
::set xmlFile=%projFolder%\..\%projName%_ideas\.project
set fileChooser=powershell "Add-Type -AssemblyName System.windows.forms|Out-Null;$f=New-Object System.Windows.Forms.OpenFileDialog;$f.InitialDirectory='!cd /D %projFolder%\..\%projName%_ideas!';$f.Filter='IDEAS project Files (*.project)|*.project';$f.showHelp=$true;$f.Multiselect=$false;$f.Title='IDEAS .project file';$f.ShowDialog()|Out-Null;if ($f.Multiselect) { $f.FileNames } else { $f.FileName }"
for /f "delims=" %%I in ('%fileChooser%') do (
	set tmpPath=%%I
	set xmlFile=!xmlFile!!tmpPath!
)

:: sublime workspace location
rem echo Select Sublime Text workspace location...
set wsPath=%projFolder%\..\%projName%_sublime
rem set wsChooser=powershell "Add-Type -AssemblyName System.windows.forms|Out-Null;$f=New-Object System.Windows.Forms.SaveFileDialog;$f.InitialDirectory='%cd%';$f.Filter='Sublime Project (*.sublime-project)|*.sublime-project';$f.showHelp=$true;$f.Title='Workspace location';$f.OverwritePrompt=$false;$f.ShowDialog()|Out-Null;$f.FileName"
rem for /f "delims=" %%I in ('%wsChooser%') do set "wsPath=%%I"
mkdir %wsPath%
set wsPath=%wsPath%\%projName%.sublime-project

:: Create Sublime workspace
echo Creating Sublime Text workspace...
(
	echo {
	echo 	"folders":
	echo 	[
	echo 		{
	echo 			"path": "!escProjFolder!",
	echo 			"folder_exclude_patterns": [".svn", ".git", ".hg", "CVS", "tstmp", "%projName%/src", "out"],
	echo 			"file_exclude_patterns": ["*.tags", "*.tags_sorted_by_file", "*.s19", "*.doc*", "*.ppt*", "*.odt", "*.swatt_log", "*.pj", "*.o", "*.lst", "*.dbo"],
	echo 			"index_exclude_patterns": []
	echo 		}
)>"!wsPath!"

set linksProjFolder=%projFolder:"=%
rem Close quote - "
set linksProjFolder=%linksProjFolder:\=/%

:: Create build config
for /f %%i in ("%wsPath%") do (
	set wsFolder=%%~dpi
	set wsName=%%~ni
)

set escSwattFolder=%SWATT_HOME:\=\\%

:: Linked resources
if NOT "%xmlFile%"=="" (
	echo "Adding linked resources (remote subprojects)..."
	for /f %%i in ('XML.EXE sel -t -v "//linkedResources/link/location" !xmlFile!') do (call :printIncludes "%%i")
)

(
	echo 	],
	echo 	"CTags":
	echo 	{
	echo 		"opts" : ["--exclude=.\\src", "--exclude=doc"]
	echo 	},
	echo 	"build_systems":
	echo 	[
	echo 		{
	echo 			"name": "Compile",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd compile $file_name",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Preprocess",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd preprocess $file_name",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Build",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd make -threads=5",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Build - No Cessar",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd make -no_step132 -threads=5",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Rebuild",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd build -threads=5",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Rebuild - No Cessar",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd build -no_step132 -threads=5",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Quality Check File",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd lint $file_name",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^(.*)\\(([0-9]*),([0-9]*)\\)"
	echo 		},
	echo 		{
	echo 			"name": "Quality Check Project",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd lintall",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^(.*)\\(([0-9]*),([0-9]*)\\)"
	echo 		},
	echo 		{
	echo 			"name": "ConfigTool",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd ConfigTool",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "MapViewer",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd mapViewer",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "Update IDEAS project",
	echo 			"working_dir": "!escProjFolder!\\source",
	echo 			"shell_cmd": "!escProjFolder!\\etools\\TsStarter.cmd update",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		},
	echo 		{
	echo 			"name": "SWATT",
	echo 			"working_dir": "$file_path",
	echo 			"shell_cmd": "!escSwattFolder!\\run_tester.bat $file_name",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^(.*?)(?:\\: |\\()([0-9]*)(?:\\:|\\))"
	echo 		},
	echo 		{
	echo 			"name": "XSLT",
	echo 			"working_dir": "$file_path",
	echo 			"selector": ["source.xsl"],
	echo 			"shell_cmd": "java -cp D:\\ts_mirr\\xml\\xalan2.6.0\\xalan.jar org.apache.xalan.xslt.Process -in cmfb_lf_tool_config.swpcfg -xsl $file_name",
	echo 			"syntax": "Packages/Makefile/Makefile.sublime-syntax",
	echo 			"file_regex": "^\"(.*^)\", line ([0-9]*)"
	echo 		}
	echo 	]
	echo }
)>>"!wsPath!"

set escWsPath=%wsPath: =^ %
echo Workspace created successfully at !escWsPath!.

rem echo Creating CScope database. Please wait...
rem cd /D !projFolder!

rem :: Add files to be indexed
rem dir /b/a/s *.c    > cscope.files 
rem dir /b/a/s *.cpp >> cscope.files
rem dir /b/a/s *.h   >> cscope.files

rem :: Escape spaces in paths
rem powershell "(Get-Content cscope.files) | ForEach-Object { $_ -replace '^(.*)$', '\"$1\"' } | Set-Content cscope.files"

rem :: Create CScope database
rem cscope -cb

rem echo CScope database created!

:: Open workspace
!escWsPath!

pause

endlocal
goto :EOF

:printIncludes
setlocal enabledelayedexpansion

set incl=%1
if "!incl:%linksProjFolder%=!"=="%incl%" (
	(
		echo 		,{
		echo 			"path": %1%
		echo 		}
	)>>"!wsPath!"
)
