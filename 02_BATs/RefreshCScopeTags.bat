@echo off
setlocal enableextensions enabledelayedexpansion

echo Select the Project root directory:
:: Project root
set projFolder=
set "psCommand="(new-object -COM 'Shell.Application').BrowseForFolder(0,'Please choose the IDEAS project root',0,0).self.path""
for /f "delims=" %%I in ('powershell %psCommand%') do (
	set tmpPath=%%I
	set projFolder=!projFolder!!tmpPath!
)

cd /D !projFolder!

:: Refresh CScope database
cscope -cb

echo Done!
pause
endlocal
