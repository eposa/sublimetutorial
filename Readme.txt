1) Install Sublime Text 3
	- https://www.sublimetext.com/

2) Launch Sublime Text and close it (needed for creating user folders).

3) Add [Sublime Text install dir] to PATH variable.

4) Copy SublimeTutorial folder to a local drive.

5) Run Setup.bat from your local drive as ADMINISTRATOR!

6) Run Sublime Text. It will scan all packages for a couple of seconds and prompt you to restart because dependencies were installed.
If there is no such prompt, go to Preferences\Package Control and type "Install Local Dependency". Restart Sublime Text

7) Run CreateWS.bat from [Sublime Text install dir] to create a new workspace.

8) With the new workspace open, rebuild CTags index (Shortcut: Ctrl+I, Ctrl+R).


---------
Hint 1: most commands are accessible from the Command Palette: Tools\Command Palette or Ctrl+Shift+P.
i.e.: For rebuilding CTags index, press Ctrl+Shift+P and type Ctags: Rebuild Tags

Hint 2: browse Preferences\Settings-Default if you want to see what can be changed. Such customization is done in Preferences\Settings-User.
i.e.:
Default setting -	"highlight_line": false,
Custom setting -	"highlight_line": true

Hint 3: browse Preferences\Key Bindings-Default if you want to see what shortcuts are assigned to commands. If you want to override a shortcut, add it in Preferences\Key Bindings-User
i.e.:
Default key binding (open resource) -	{ "keys": ["ctrl+r"], "command": "show_overlay", "args": {"overlay": "goto", "show_files": true} }
Custom key binding (Eclipse-like) - 	{ "keys": ["ctrl+shift+r"], "command": "show_overlay", "args": {"overlay": "goto", "show_files": true} }

Hint 4: common commands, with current shortcuts:
	ctrl+shift+r: open resource
	ctrl+l: go to line
	ctrl+p: go to symbol
	ctrl+d: delete line
	ctrl+shift+f: find/replace in files
	ctrl+b: build
	alt+left: go backwards
	alt+right: go forwards
	ctrl+tab: switch c/h file
	F3: go to definition
	alt+b: go back (after going to definition)
	ctrl+i, ctrl+r: rebuild CTags index
	`+b: show build results
	`+s: toggle sidebar
	`+m: toggle minimap
	ctrl+t: open CTags menu
	F11: toggle fullscreen mode
	shift+F11: toggle distraction-free mode
