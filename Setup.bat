@echo off
setlocal enableextensions enabledelayedexpansion

:: CD to correct dir
cd /D "%~dp0"

:: Sublime install dir
echo Select Sublime Text install directory...

set sublInstallDir=
rem C:\Program Files\Sublime Text 3
set "psCommand="(new-object -COM 'Shell.Application').BrowseForFolder(0,'Select Sublime Text install directory',0,0).self.path""
for /f "delims=" %%I in ('powershell %psCommand%') do (
	set tmpPath=%%I
	set sublInstallDir=!sublInstallDir!!tmpPath!
)

:: AppData dir
echo Select User directory (C:\Users\[your uid]\)...

set appDataDir=
rem C:\Users\uidw2660
set "psCommand="(new-object -COM 'Shell.Application').BrowseForFolder(0,'Select your User directory',0,0).self.path""
for /f "delims=" %%I in ('powershell %psCommand%') do (
	set tmpPath=%%I
	set appDataDir=!appDataDir!!tmpPath!
)
set appDataDir=!appDataDir!\AppData\Roaming\Sublime Text 3

:: Install Visual C++ Redistributable
start /WAIT 00_EXEs/vc_redist.x64.exe

:: Copy EXEs
echo Copying EXEs...
xcopy /s 00_EXEs "%sublInstallDir%"

:: Copy AppData
echo Copying AppData...
xcopy /s 01_AppData "%appDataDir%"

:: Copy CreateWS script
echo Copying scripts...
xcopy /s 02_BATs "%sublInstallDir%"

:: Copy CppCheck files
echo Copying scripts...
xcopy /s 03_CppCheck "%sublInstallDir%"

echo Setup complete!
pause
